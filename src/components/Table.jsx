import React, { useState, useEffect } from 'react';
import axios from 'axios';
import "./table-style.css"
import PopupCard from './PopupCard';
import UpdatePopupCard from './UpdatePopupCard';

const ProductTable = () => {
  const [url, setUrl] = useState("");
  const [selectedProduct, setSelectedProduct] = useState([]);
  const [products,setProducts] = useState([])
  const [isPopupOpen, setPopupOpen] = useState(false);
  const [isUpdatePopupOpen, setUpdatePopupOpen] = useState(false);
  const [searchKey , setSearchKeys] = useState("")
  const [category,setCategory] = useState("all")


  useEffect(() => {
    const fetchProducts = async () => {
        try {
          const response = await axios.get(
            'https://api.postman.com/collections/24582109-37d97559-22b0-42e0-b592-7fd8b90b8e01?access_key=PMAT-01GXAEX88FNRZN45AWACQ2V20F'
          );
          let apiUrl = response.data.collection.item[0].request.url.raw 
          // console.log(response.data.collection.item[0].request.url.raw);  
          setUrl(apiUrl);   
          randomFun(apiUrl)
        } catch (error) {
          console.error('Error fetching products:', error);
        }
      };
    fetchProducts();


    let randomFun = async (apiUrl) => {
      try {
        const response = await axios.get(apiUrl);
         setProducts([response.data]);
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };
  }, []);






  const togglePopup = async(prod) => {
  setSelectedProduct(prod)
   setPopupOpen(!isPopupOpen);
   console.log("View Item :- ", prod);
  };





  const updatePopup = (product)=>{
    console.log(product);
    setSelectedProduct(product)
    setUpdatePopupOpen(!isUpdatePopupOpen)
  }
  const handleUpdate = async () => {
    try {
      const product = selectedProduct
      await axios.put(`${url}/${product.id}`, product);
      console.log('Product updated:', product);
    } catch (error) {
      console.error("Error in updating" , error);
    }
  };
  const handleUpdateClose = () =>{
    setUpdatePopupOpen(!isUpdatePopupOpen)
  }

  const handleDelete = async (product) => {
    try {
     let deletedProduct = products[0].filter((item,seq)=>{
                return product.id===item.id
      })
      console.log("deleted Product :-" , deletedProduct);
      let delProd = deletedProduct[0]
      let allProd = products[0].filter((item,index)=>{
        return item.id!==delProd.id
      })
      setProducts([allProd])
      await axios.delete(`${url}/${delProd.id}` ,products);
    } catch (error) {
      console.error('Error deleting product:', error);
    }
  };

  return (
    <div>
      <input type="text" placeholder="Search products" onChange={(e)=>setSearchKeys(e.target.value)} />
      <select name="categories" value={category} onChange={(e)=>{setCategory(e.target.value)}} >
        <option value="all categories" >All</option>
        <option value="men's clothing">Men's clothing</option>
        <option value="jewelery">jewelery</option>
        <option value="electronics">electronics</option>
        <option value="women's clothing">women's clothing</option>
      </select>
      <div className="table-container">
  <table className="product-table">
    <thead>
      <tr className='head-tr'>
        <th>Product Title</th>
        <th>Product Price</th>
        <th>Product Description</th>
        <th>Product Category</th>
        <th className='head-tr'>Action</th>
      </tr>
    </thead>
    <tbody>
    {
  products.map((item, index) => {
    return (
      item.filter((val)=>{
        if(searchKey==="" && category==="all"){
          return val
        }
        else if(val.title.toLowerCase().includes(searchKey.toLowerCase()) && category==="all"){
          return val
        }
        else if(val.title.toLowerCase().includes(searchKey.toLowerCase()) && val.category=== category ){
          return val
        }
      }).map((prod,seq)=>{
        return(
          <tr key={seq}>
        <td>{prod.title}</td>
        <td>{prod.price}</td>
        <td>{prod.description}</td>
        <td>{prod.category}</td>
        <td>
          <button className="btn-view" onClick={(seq)=>{togglePopup(prod)}}>View</button>
          <button className="btn-update" onClick={()=>{ updatePopup(prod) }}>Update</button>
          <button className="btn-delete" onClick={()=>{handleDelete(prod)}} >Delete</button>
        </td>
      </tr>
        )
      })
    );
  })
}

    </tbody>
  </table>
</div>
      <PopupCard isPopupOpen = {isPopupOpen} selectedProduct={selectedProduct} setPopupOpen = {setPopupOpen} togglePopup={togglePopup}/>
      <UpdatePopupCard handleUpdate={handleUpdate} handleUpdateClose={handleUpdateClose} selectedProduct={selectedProduct} isUpdatePopupOpen={isUpdatePopupOpen} setUpdatePopupOpen={setUpdatePopupOpen}/>
    </div>
  );
};


export default ProductTable;

         
