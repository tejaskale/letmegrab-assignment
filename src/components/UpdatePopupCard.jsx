import React, { useState } from 'react';
import "./popup-styles.css";

const UpdatePopupCard = (props) => {



  return (
    <div>
      {props.isUpdatePopupOpen && (
        <div className="popup-container">
          <div className="popup-card">
            <div className='inp-box-container' >
                <label htmlFor="title">Title : </label>
                <input type="text" defaultValue={props.selectedProduct.title} onChange={(e)=>{props.selectedProduct.title = e.target.value}} />
            </div>
            <div className='inp-box-container'>
                <label htmlFor="">Description : </label>
                <textarea rows="4" cols="30" type="text" defaultValue={props.selectedProduct.description} onChange={(e)=>{props.selectedProduct.description = e.target.value}} />
            </div>
            <div className='inp-box-container'>
                <label htmlFor="category">Category :</label>
                <input type="text" defaultValue={props.selectedProduct.category} onChange={(e)=>{props.selectedProduct.category = e.target.value}}/>
            </div>
            <div className='inp-box-container'>
                <label htmlFor="Price">Price : </label>
                <input type="text" defaultValue={props.selectedProduct.price} onChange={(e)=>{props.selectedProduct.price = e.target.value ;}} />
            </div>
            <div className='inp-box-container'>
            <button onClick={()=>{props.handleUpdate()}} >Update</button>
            <button onClick={()=>{props.handleUpdateClose()}} >Close</button>
            </div>  
          </div>
        </div>
      )}
    </div>
  );
};

export default UpdatePopupCard;
