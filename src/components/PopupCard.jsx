import React, { useState } from 'react';
import "./popup-styles.css";

const PopupCard = (props) => {

  return (
    <div>
      {props.isPopupOpen && (
        <div className="popup-container">
          <div className="popup-card">
            <h2>
              {props.selectedProduct.title}
            </h2>
            <p>{props.selectedProduct.description}</p>
            <h4> Category: {props.selectedProduct.category}</h4>
            <h3>Price : {props.selectedProduct.price}</h3>
            <button onClick={()=>{props.setPopupOpen(!props.isPopupOpen)}} >Close</button>
          </div>
        </div>
      )}
    </div>
  );
};
export default PopupCard;